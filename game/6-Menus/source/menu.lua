Menu = {} --comecamos criando um registro das propriedades do menu
Menu.__index = Menu

setmetatable(Menu, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})
  
function Menu.new(backgroundImage)
  local self = setmetatable({}, Menu)  
  self.font = love.graphics.newFont(20) --criamos uma fonte padrao para o menu  
  self.buttons = {} --criamos um registro para os botoes  
  self.backImage = love.graphics.newImage(backgroundImage) --carrega uma imagem para o fundo do menu
  return self
end

function Menu:addButton(text, x, y, id) --funcao que cria botoes, criaremos dois somente
	table.insert(self.buttons, {text=text, x=x, y=y, id=id, selected=false})
	--table insert eh uma funcao da lua para preencher uma tabela
	--text sera mensagem do botao
	--x e y sua posicao na tela
	--id sua identidade
	--e selected uma varivel para verificacao
	self.buttons[1].selected = true -- o primeiro botao ja surge selecionado
end

function Menu:draw () --essa funcao percorre o registro criado e desenha cada botao
	love.graphics.setColor(250, 250, 250)
  love.graphics.draw(self.backImage, 0, 0)
  for i,v in ipairs(self.buttons) do --ipairs eh um comando para retornar do registro um conjunto de valores associados a uma posicao
		love.graphics.setColor(0, 0, 0) --cor preta
		love.graphics.rectangle("line", 335, v.y-5, 125, self.font:getHeight()+10) --desenha as bordas do botao
		if v.selected then
      --quando o botao esta selecionado ficara com a cor rosa clara
			love.graphics.setColor(204, 0, 204) 
      --desenha um botao com tamanho suficiente para caber a mensagem
			love.graphics.rectangle("fill", 335, v.y-5, 125, self.font:getHeight()+10) 
		else
			love.graphics.setColor(102, 0, 102) --quando o botao nao esta selecionado ficara com a cor rosa escuro
			love.graphics.rectangle("fill", 335, v.y-5, 125, self.font:getHeight()+10)
		end
		love.graphics.setColor(255, 255, 255)--define a cor da mensagem
		love.graphics.setFont(self.font)--chama a fonte padrao
		love.graphics.print(v.text, v.x, v.y) --escreve a mensagem nas posicoes escolhidas
	end
end

function Menu:checkSelection(key) --verfica se o botao esta em selecao
	if key=="up" then --caso o usuario aperte a tecla UP selecionara o botao de cima e os outros nao
		self.buttons[1].selected = true
		self.buttons[2].selected = false
	elseif key=="down" then --caso o usuario aperte a tecla DOWN selecionara o botao de baixo e os outros nao
		self.buttons[1].selected = false
		self.buttons[2].selected = true
	end
end
