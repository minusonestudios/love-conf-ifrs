Caixa = {}
Caixa.__index = Caixa

setmetatable(Caixa, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})
  
local function buildBorder(world, x1, x2, y1, y2)
  local border = {}
  --cria um corpo para a parede, no mundo do jogo e do tipo static (ou seja, nao se movimenta)
  border.body = love.physics.newBody(world, 0, 0, "static")
  --cria uma forma para a parede, nas coordenadas definidas
  border.shape = love.physics.newEdgeShape(x1, x2, y1, y2)
  --fixa a forma ao corpo, criando a area de colisao
  border.fixture = love.physics.newFixture(border.body, border.shape, 5)
  return border;
end
	
function Caixa.new(world, scenario)
  local self = setmetatable({}, Caixa) --cria um registro com as caracteristicas da Caixa
  
  local width = scenario:getWidth()
  local height = scenario:getHeight()
  
  self.borders = {
      top = buildBorder(world, 5, 5, width, 5)
    , left = buildBorder(world, 5, 5, 5, height)
    , right = buildBorder(world, width-120, 5, width-120, height)
    , bottom = buildBorder(world, 5, height, width, height)
  }
  
  return self
end

--desenho das bordas, opcional, usado somente para verificar se as posicoes delas estao corretas
function Caixa:draw()
	for i, border in ipairs(self.borders) do --mesma mecânica que um "for each"
		love.graphics.setColor(255, 0, 0)
		love.graphics.line(border.body:getWorldPoints(border.shape:getPoints()))
		--desenha o limite como uma linha, body:getWorldPoint e o shape:getPoints retornam os valores definidos da parede na criacao
	end
end
