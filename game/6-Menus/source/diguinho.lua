local anim8 = require 'source/anim8' --importa a biblioteca do love responsvel pelas animaes

Jogador = {}
Jogador.__index = Jogador

setmetatable(Jogador, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})

function Jogador.new(world) --instancia as propriedades do jogador
	--mesmo processo feito com o jogador
	local self = setmetatable({}, Jogador) --criamos um registro para guardar as informacoes do jogador
	self.body = love.physics.newBody(world, 200, love.graphics.getWidth()/2, "dynamic")--criamos o corpo dele no mundo, na posicao (100,300) do tipo dinamico, ou seja ele se movimenta
	self.shape = love.physics.newCircleShape(30) --cria uma hit 'box' para o jogador, na verdade eh um circulo como area de contato
	self.fixture = love.physics.newFixture(self.body, self.shape, 1)--unimos o corpo com a area de contato
	self.alive = true --cria uma variavel que carrega a informacao que o jogador esta vivo ou morto
  self.image = love.graphics.newImage("imagens/diguinho.png") --carrega o sprite de animacao
  self.image:setFilter("nearest", "nearest") --define o filtro de interpolacao da imagem, mudando sua textura
	local frames = anim8.newGrid(128, 128, self.image:getWidth(), self.image:getHeight())
	self.animation = anim8.newAnimation(frames('1-3',1), 0.05) --cria a animacao do jogador, que troca de frames entre 3 frames ('1-3'), a cada 0.05seg
  
  return self
end

function Jogador:update(dt) --funcao que atualiza o jogador, a sua animacao e sua movimentacao
	self.animation:update(dt) 
	--certas operacoes sao podem ocorrer com o jogador vivo, pois com ele morto alem de nao fazerem sentido, ocorrerah um erro
	if self.alive then
    if love.keyboard.isDown("right","left","up","down") then
      if love.keyboard.isDown("right") and love.keyboard.isDown("up") then
        self.body:setLinearVelocity(200, -200)
      elseif love.keyboard.isDown("left") and love.keyboard.isDown( "up") then
        self.body:setLinearVelocity(-200, -200)
      elseif love.keyboard.isDown("left") and love.keyboard.isDown( "down") then
        self.body:setLinearVelocity(-200, 200)
      elseif love.keyboard.isDown("right") and love.keyboard.isDown( "down") then
        self.body:setLinearVelocity(200, 200)
      elseif love.keyboard.isDown("right") then
        self.body:setLinearVelocity(200, 0)
      elseif love.keyboard.isDown("left") then
        self.body:setLinearVelocity(-200, 0)
      elseif love.keyboard.isDown("up") then
        self.body:setLinearVelocity(0, -200)
      elseif love.keyboard.isDown("down") then
        self.body:setLinearVelocity(0, 200)
      end
    else
      self.body:setLinearVelocity(0,0) --caso o usuario nao esteja pressionando nenhuma tecla o jogador para
    end
	end
end

function Jogador:draw() --essa funcao desenha a animacao do jogador e sua forma se quiser conferir o posicionamento
	if self.alive then
		love.graphics.setColor(255, 255, 255,255)--definimos a cor branca para preservar a cor original da imagem
		--desenhamos a animacao do jogador
		self.animation:draw(self.image
                       --fazemos este calculo para centralizar a animacao em relacao ao corpo "fisico" do jogador     
							         , self.body:getX() - self.shape:getRadius() * 2 
							         , self.body:getY() - self.shape:getRadius() * 2) 	
						
		--desenhamos a area de contato para posiciona-la adequadamente, depois comentamos
		--love.graphics.setColor(255, 0, 255)
		--love.graphics.circle("line", self.body:getX(), self.body:getY(), self.shape:getRadius())
	end
end
