require('source/camera') ---biblioteca pronta para camera seguir o jogador
require('source/diguinho') --personagem principal
require('source/flor') --ponto final da fase
require('source/caixa') --bordas do cenario

local jogador = {}
local flor = {}
local caixa = {}
local world = nil
local scenario = nil
local font = nil
local show = 0
local width = 0
local height = 0

--[[-----------------------------------------------------------------------------]]
--[[----------------------- Funcoes privados do jogo ----------------------------]]
--[[-----------------------------------------------------------------------------]]

--Funcao exlusiva para o uso da biblioteca 'camera'
function math.clamp(x, min, max) 
	return x < min and min or (x > max and max or x)
end

--Funcao que carrega a fase no jogo
local function iniciarJogo ()
	--cria um mundo para o jogo, com fisicas, como por exemplo, gravidade (horizontal ou vertical)
	love.physics.setMeter(64) --define o numero de pixels/metro no mundo
	world = love.physics.newWorld( 0, 100*64, true )
	--cria funcoes que sao chamadas quando corpos no Mundo se colidem
	world:setCallbacks(beginContact, function() collectgarbage() end)
	--a funcao colletgarbage eh, uma funcao da Lua para limpar a memoria de objetos que foram destruidos

	--Cria a camera que seguira o jogador
	--camera
	width = love.graphics.getWidth()
	height = love.graphics.getHeight()
	camera:setBounds(0, 0, width, height)

	--'show' eh a variavel que controla o alpha do backgroud, ou seja a transparencia, 0 eh transparente e 255 eh opaco
	show = 255

	--funcoes que criarao os elementos do jogo
	jogador = Jogador(world)
	flor = Flor(world)
  	caixa = Caixa(world, scenario)
end

--[[-----------------------------------------------------------------------------]]
--[[------------------ Eventos do Love - Maquina de Estados ---------------------]]
--[[-----------------------------------------------------------------------------]]

--Funcao principal Load (soh roda 1 vez)
function love.load()

	love.graphics.setBackgroundColor( 0, 0, 0 ) --muda a cor de fundo

	--define um tamanho de fonte padrao para ser usado no jogo
	font = love.graphics.newFont(72)

	scenario = love.graphics.newImage( 'imagens/fundo.png' ) --carrega uma imagem para o background

	--agora vamos carregar o primeiro nivel do jogo
	iniciarJogo()
end

--Funcao principal Update que atualiza o jogo a cada frame
function love.update(dt) --dt = delta time (tempo entre um frame e outro)
  	world:update(dt)
  	jogador:update(dt)
	-- essa funcao eh da camera, que faz com q ela siga o jogador
  	camera:setPosition(jogador.body:getX() - width / 2, jogador.body:getY() - height / 2)
end

--Funcao principal de desenho dos objetos
function love.draw()
  	camera:set() --foca a camera no jogador
  	love.graphics.setColor(250, 250, 250,show)
  	love.graphics.draw(scenario, 0, 0)
  	flor:draw()
	jogador:draw()
	caixa:draw()  
	camera:unset()--desfoca a camera da posicao antiga
end
