local anim8 = require 'source/anim8' --importa a biblioteca do love responsvel pelas animaes

Coruja = {}
Coruja.__index = Coruja

setmetatable(Coruja, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})
	  
function Coruja.new(world) -- carrega as propriedades do inimigo
	
	local self = setmetatable({}, Coruja) --cria um registro com as caracteristicas da coruja
  self.size = 320	
  --cria um corpo para a coruja no limite esquerdo do mapa e no topo, a cima do jogador, o tipo kinematic significa que ela s colide com corpos dynamic, no caso s com o jogador
	self.body = love.physics.newBody(world, -self.size, 0, "kinematic")
	--criamos entao sua area de contato, vai ser o dobro da do jogador
	self.shape = love.physics.newCircleShape(60)
	--unimos o corpo a area de contato
	self.fixture = love.physics.newFixture(self.body, self.shape, 3)
  self.image = love.graphics.newImage("imagens/corujaImg.png") --carrega o sprite da coruja
  self.image:setFilter("nearest", "nearest") --seta o filtro de imagem sobre o sprite

  --animao
	local frames = anim8.newGrid(self.size, self.size, self.image:getWidth(), self.image:getHeight())
  --cria a animao do inimigo, que troca entre 5 frames ('1-5'), a cada 0.15seg
	self.animation = anim8.newAnimation(frames('1-5',1), 0.15) 

	--vx e vx so as variveis que recebero uma velocidade em determinada direo para fazer a coruja se movimentar
	self.vx = 0
	self.vy = 0

  return self
end

function Coruja:update(dt, scenario) --atualiza a animao da coruja e sua movimentao
  self.animation:update(dt)
  -- as seguintes condies fazem o movimento vertical da coruja, que se move da borda superior ate a inferior e vice-versa
  if self.body:getY() >= scenario:getHeight() then
		--quando ela chega a porta inferior, ela sobe		
    self.vy = -800

  elseif self.body:getY() <= 0 then
		--quando ela chega a porta superior ela desce
    self.vy = 800
		end

  --as seguintes condies combinadas com as de cima faro a coruja se movimentar em zig-zag pelo cenario, vx aumenta proporcionalmente ao dt(tempo)
  if self.body:getX() <= scenario:getWidth() then
    self.vx = self.vx + 100*dt --caso a coruja esteja localizada antes do largura da tela ela continuara se movento para direita
		else
    self.vx = self.vx - 200*dt --caso tenha passado da largura da tela ela voltar rapidamente para alcanar o jogador
		end

  self.body:setLinearVelocity(self.vx, self.vy) --adciona os valores finais de vx e vy a velocidade
	end

function Coruja:draw() --aqui desenhamos a coruja e sua area de contato
	love.graphics.setColor(255, 255, 255)
	self.animation:draw(self.image, self.body:getX()-150, self.body:getY()-160)
	--love.graphics.setColor(255, 5,5,255) --set the drawing color to red for the ball
	--love.graphics.circle("line", self.body:getX(), self.body:getY(), self.shape:getRadius())
end
