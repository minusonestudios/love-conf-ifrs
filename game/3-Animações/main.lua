require('source/camera') ---biblioteca pronta para camera seguir o jogador
require('source/diguinho') --personagem principal
require('source/flor') --ponto final da fase
require('source/caixa') --bordas do cenario

local jogador = {}
local flor = {}
local caixa = {}
local world = nil
local scenario = nil
local font = nil
local gamestate = ''
local show = 0
local width = 0
local height = 0

--[[-----------------------------------------------------------------------------]]
--[[----------------------- Funcoes privados do jogo ----------------------------]]
--[[-----------------------------------------------------------------------------]]

--Funcao exlusiva para o uso da biblioteca 'camera'
function math.clamp(x, min, max) 
	return x < min and min or (x > max and max or x)
end

--Funcao que termina o jogo caso o jogador esteja morto
local function onEnd()
	if not jogador.alive then
		show=0 --seta o alpha de scenario para 0, assim as imagens de scenario sumirao
		camera:setPosition(0,0) --seta a posicao da camera para o vertice superior esquerdo
	end
end

--Funcao que eh chamada quando dois corpos entram em contato,
--'a'eh o primeiro corpo
--'b' o segundo
--'coll' eh o tipo de colisao
local function beginContact(a, b, coll)
	--colisao do jogador com o solo
	if jogador.alive and b==jogador.fixture and a == caixa.borders.bottom.fixture then
		jogador.alive = false
		jogador.body:destroy()
		gamestate = 'gameover'
	end

	--colisao do jogador com a flor
	if jogador.alive and (a ==jogador.fixture and b == flor.fixture)
	or  (b ==jogador.fixture and a == flor.fixture) then
		jogador.alive = false
		gamestate = 'levelup'
	end
end

--Funcao que carrega a fase no jogo
local function iniciarJogo ()
	--cria um mundo para o jogo, com fisicas, como por exemplo, gravidade (horizontal ou vertical)
	love.physics.setMeter(64) --define o numero de pixels/metro no mundo
	world = love.physics.newWorld( 0, 100*64, true )
	--cria funcoes que sao chamadas quando corpos no Mundo se colidem
	world:setCallbacks(beginContact, function() collectgarbage() end)
	--a funcao colletgarbage eh, uma funcao da Lua para limpar a memoria de objetos que foram destruidos

	--Cria a camera que seguira o jogador
	--camera
	width = love.graphics.getWidth()
	height = love.graphics.getHeight()
	camera:setBounds(0, 0, width, height)

	--'show' eh a variavel que controla o alpha do backgroud, ou seja a transparencia, 0 eh transparente e 255 eh opaco
	show = 255

	--funcoes que criarao os elementos do jogo
	jogador = Jogador(world)
	flor = Flor(world)
  caixa = Caixa(world, scenario)
end

--[[-----------------------------------------------------------------------------]]
--[[------------------ Eventos do Love - Maquina de Estados ---------------------]]
--[[-----------------------------------------------------------------------------]]

--Funcao principal Load (soh roda 1 vez)
function love.load()

	love.graphics.setBackgroundColor( 0, 0, 0 ) --muda a cor de fundo

	--define um tamanho de fonte padrao para ser usado no jogo
	font = love.graphics.newFont(72)

	scenario = love.graphics.newImage( 'imagens/fundo.png' ) --carrega uma imagem para o background
	
	gamestate = 'playing' --define o primeiro estado do jogo como o 'playing'

	--agora vamos carregar o primeiro nivel do jogo
	iniciarJogo()
end

--Funcao principal Update que atualiza o jogo a cada frame
function love.update(dt) --dt = delta time (tempo entre um frame e outro)
	if gamestate == 'playing' then --caso a fase tenha comecado, os seguintes elementos sao atualizados
		world:update(dt)    
    if jogador.alive then
      jogador:update(dt)
      -- essa funcao eh da camera, que faz com q ela siga o jogador
      camera:setPosition(jogador.body:getX() - width / 2, jogador.body:getY() - height / 2) 
    end
		--funcao que verifica se o jogador conluiu a fase ou morreu
		onEnd()
	end
end

--Funcao principal de desenho dos objetos
function love.draw()
	camera:set() --foca a camera no jogador
	if jogador.alive then --verifica se o jogador esta vivo, se sim, desenha todos os elementos associados a ele
		love.graphics.setColor(250, 250, 250, show)
		love.graphics.draw(scenario, 0, 0)
		flor:draw()
		jogador:draw()
		caixa:draw()
	end
	love.graphics.setFont(font) --chama a fonte definida no load para ser usada
	if gamestate=='gameover' then --se o jogador morreu, escreve uma mensagem para o usuario
		love.graphics.setColor(255, 255, 255)
		love.graphics.printf("GAME OVER", 200, 200, 400, "center")
	elseif gamestate=='levelup' then --se o jogador venceu, escreve outra mensagem para o usuario
		love.graphics.setColor(255, 255, 255)
		love.graphics.printf("YOU WIN!", 200, 250, 400, "center")
	end
	camera:unset()--desfoca a camera da posicao antiga
end
