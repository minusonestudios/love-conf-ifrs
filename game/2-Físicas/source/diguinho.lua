Jogador = {}
Jogador.__index = Jogador

setmetatable(Jogador, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})

function Jogador.new(world) --instancia as propriedades do jogador
	--mesmo processo feito com o jogador
	local self = setmetatable({}, Jogador) --criamos um registro para guardar as informacoes do jogador
	self.body = love.physics.newBody(world, 200, love.graphics.getWidth()/2, "dynamic")--criamos o corpo dele no mundo, na posicao (100,300) do tipo dinamico, ou seja ele se movimenta
	self.shape = love.physics.newCircleShape(30) --cria uma hit 'box' para o jogador, na verdade eh um circulo como area de contato
	self.fixture = love.physics.newFixture(self.body, self.shape, 1)--unimos o corpo com a area de contato
	self.alive = true --cria uma variavel que carrega a informacao que o jogador esta vivo ou morto
  return self
end

function Jogador:update(dt) --funcao que atualiza o jogador e sua movimentacao
	--certas operacoes sao podem ocorrer com o jogador vivo, pois com ele morto alem de nao fazerem sentido, ocorrerah um erro
	if self.alive then
    if love.keyboard.isDown("right","left","up","down") then
      if love.keyboard.isDown("right") and love.keyboard.isDown("up") then
        self.body:setLinearVelocity(200, -200)
      elseif love.keyboard.isDown("left") and love.keyboard.isDown( "up") then
        self.body:setLinearVelocity(-200, -200)
      elseif love.keyboard.isDown("left") and love.keyboard.isDown( "down") then
        self.body:setLinearVelocity(-200, 200)
      elseif love.keyboard.isDown("right") and love.keyboard.isDown( "down") then
        self.body:setLinearVelocity(200, 200)
      elseif love.keyboard.isDown("right") then
        self.body:setLinearVelocity(200, 0)
      elseif love.keyboard.isDown("left") then
        self.body:setLinearVelocity(-200, 0)
      elseif love.keyboard.isDown("up") then
        self.body:setLinearVelocity(0, -200)
      elseif love.keyboard.isDown("down") then
        self.body:setLinearVelocity(0, 200)
      end
    else
      self.body:setLinearVelocity(0,0) --caso o usuario nao esteja pressionando nenhuma tecla o jogador para
    end
	end
end

function Jogador:draw() --essa funcao desenha a animacao do jogador e sua forma se quiser conferir o posicionamento
	if self.alive then				
		--desenhamos a area de contato para posiciona-la adequadamente, depois comentamos
		love.graphics.setColor(255, 0, 255)
		love.graphics.circle("line", self.body:getX(), self.body:getY(), self.shape:getRadius())
	end
end
