Flor = {}
Flor.__index = Flor

setmetatable(Flor, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})

function Flor.new(world) --criao da flor, o objetivo final do jogo
	--mesmo processo feito com o jogador
	local self = setmetatable({}, Flor)
	self.body = love.physics.newBody(world, 1300,800, "static")--no caso da flor a area de contato ser s o miolo
	self.shape = love.physics.newCircleShape(30)
	self.fixture = love.physics.newFixture(self.body, self.shape, 1)    
  return self
end

function Flor:draw() --desenhamos a flor
	love.graphics.setColor(0, 0, 0, 255)
	love.graphics.circle("line",self.body:getX(),self.body:getY(),self.shape:getRadius())
end
